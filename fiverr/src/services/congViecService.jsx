import axios from "axios";
import { DOMAIN, createConfig } from "../utils/config";

export const congViecService = {
  layDanhSachCongViecTheoTen: (tenCongViec) => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${tenCongViec}`,
      method: "GET",
      data: tenCongViec,
      headers: createConfig(),
    });
  },

  layMenuLoaiCongViec: () => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-menu-loai-cong-viec`,
      method: "GET",
      headers: createConfig(),
    });
  },

  layCongViecTheoChiTietLoai: (maLoaiCongViec) => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-chi-tiet-loai-cong-viec/${maLoaiCongViec}`,
      method: "GET",
      headers: createConfig(),
    });
  },

  layCongViecChiTiet: (id) => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-cong-viec-chi-tiet/${id}`,
      method: "GET",
      headers: createConfig(),
    });
  },

  layBinhLuanTheoCongViec: (id) => {
    return axios({
      url: `${DOMAIN}/api/binh-luan/lay-binh-luan-theo-cong-viec/${id}`,
      method: "GET",
      headers: createConfig(),
    });
  },

  binhLuan: (data) => {
    return axios({
      url: `${DOMAIN}/api/binh-luan`,
      method: "POST",
      data: data,
      headers: createConfig(),
    });
  },
};
