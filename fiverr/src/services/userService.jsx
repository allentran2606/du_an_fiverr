import axios from "axios";
import { createConfig, DOMAIN } from "../utils/config";

export const userService = {
  layThongTinUserDangNhap: (dataUser) => {
    return axios({
      url: `${DOMAIN}/api/auth/signin`,
      method: "POST",
      headers: createConfig(),
      data: dataUser,
    });
  },

  layThongTinUserDangKy: (dataUser) => {
    return axios({
      url: `${DOMAIN}/api/auth/signup`,
      method: "POST",
      headers: createConfig(),
      data: dataUser,
    });
  },

  fetchUsersData: () => {
    return axios({
      url: `${DOMAIN}/api/users`,
      method: "GET",
      headers: createConfig(),
    });
  },

  fetchUserById: (id) => {
    return axios({
      url: `${DOMAIN}/api/users/${id}`,
      method: "GET",
      headers: createConfig(),
    });
  },

  deleteUser: (id) => {
    return axios({
      url: `${DOMAIN}/api/users?id=${id}`,
      method: "DELETE",
      headers: createConfig(),
    });
  },

  addUser: (data) => {
    return axios({
      url: `${DOMAIN}/api/users`,
      method: "POST",
      headers: createConfig(),
      data: data,
    });
  },

  editUser: (data) => {
    return axios({
      url: `${DOMAIN}/api/users/${data.id}`,
      method: "PUT",
      headers: createConfig(),
      data: data,
    });
  },

  layNguoiDungTheoId: (id) => {
    return axios({
      url: `${DOMAIN}/api/users/${id}`,
      method: "GET",
      headers: createConfig(),
    });
  },

  layDanhSachDaThue: (id) => {
    return axios({
      url: `${DOMAIN}/api/thue-cong-viec/lay-danh-sach-da-thue`,
      method: "GET",
    });
  },

  layThongTinUserDangNhap: (dataUser) => {
    return axios({
      data: dataUser,
      url: `${DOMAIN}/api/auth/signin`,
      method: "POST",
      headers: createConfig(),
    });
  },

  thueCongViec: (data) => {
    return axios({
      url: `${DOMAIN}/api/thue-cong-viec`,
      method: "POST",
      data: data,
      headers: createConfig(),
    });
  },
};
