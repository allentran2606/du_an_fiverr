import { configureStore } from "@reduxjs/toolkit";
import congViecSlice from "./slice/congViecSlice";
import userSlice from "./slice/userSlice";

const store = configureStore({
  reducer: {
    congViecSlice,
    userSlice,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;
