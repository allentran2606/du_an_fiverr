import { createAsyncThunk } from "@reduxjs/toolkit";
import { userLocalStorage } from "../../services/localStorageService";
import { userService } from "../../services/userService";
import { message } from "antd";

export const postDangNhap = createAsyncThunk("postDangNhap", async (values) => {
  try {
    const userData = await userService.layThongTinUserDangNhap(values);
    // Lưu vào local storage
    userLocalStorage.set(userData.data.content);
    return userData.data.content;
  } catch ({ message }) {
    console.log(`message: `, message);
  }
});

export const postDangKy = createAsyncThunk("postDangKy", async (values) => {
  console.log(values);
  try {
    const userData = await userService.layThongTinUserDangKy(values);
    userLocalStorage.set(userData.data.content);
    console.log(userData.data.content);
    message.success("Sign up success!");
    return userData.data.content;
  } catch ({ message }) {
    console.log(`message: `, message);
  }
});

export const getUsersData = createAsyncThunk("getUsersData", async () => {
  try {
    const request = await userService.fetchUsersData();
    return request.data.content;
  } catch (error) {
    console.error(error);
  }
});

export const getUserById = createAsyncThunk("getUserById", async (id) => {
  try {
    const request = await userService.fetchUserById(id);
    console.log(request.data.content);
    return request.data.content;
  } catch (error) {
    console.error(error);
  }
});

export const addUser = createAsyncThunk("addUser", async (newUser) => {
  try {
    const request = await userService.addUser(newUser);
    console.log("success");
    return request.data.content;
  } catch (error) {
    console.log("failed");
    console.error(error);
  }
});

export const deleteUser = createAsyncThunk("deleteUser", async (id) => {
  try {
    const request = await userService.deleteUser(id);
    return id; // payload creator (what we return from this async fn will be the payload of the extrareducers)
  } catch (error) {
    console.error(error);
  }
});

export const updateUser = createAsyncThunk(
  "updateUser",
  async (updatedUser) => {
    try {
      const request = await userService.editUser(updatedUser);
      console.log("success");
      console.log(request.data.content);
      return request.data.content;
    } catch (error) {
      console.log("failed");
      console.error(error);
    }
  }
);

export const getUserTheoID = createAsyncThunk("getUserTheoID", async (id) => {
  try {
    const request = await userService.layNguoiDungTheoId(id);
    console.log(request.data.content);
    return request.data.content;
  } catch (error) {
    console.log(error);
  }
});

export const getDanhSachDaThue = createAsyncThunk(
  "getDanhSachDaThue",
  async (id) => {
    try {
      const request = await userService.layDanhSachDaThue();
      console.log(request.data.content);
      return request.data.content;
    } catch (error) {
      console.log(error);
    }
  }
);
