import { createAsyncThunk } from "@reduxjs/toolkit";
import { congViecService } from "../../services/congViecService";

export const getDanhSachCongViecTheoTen = createAsyncThunk(
  "getDanhSachCongViecTheoTen",
  async (tenCongViec) => {
    try {
      const jobData = await congViecService.layDanhSachCongViecTheoTen(
        tenCongViec
      );
      return jobData.data.content;
    } catch ({ message }) {}
  }
);

export const getMenuLoaiCongViec = createAsyncThunk(
  "getMenuLoaiCongViec",
  async () => {
    try {
      const content = await congViecService.layMenuLoaiCongViec();
      return content.data.content;
    } catch ({ messsage }) {}
  }
);

export const getCongViecTheoChiTietLoai = createAsyncThunk(
  "getCongViecTheoChiTietLoai",
  async (maChiTietLoai) => {
    try {
      const content = await congViecService.layCongViecTheoChiTietLoai(
        +maChiTietLoai
      );
      return content.data.content;
    } catch ({ message }) {}
  }
);

export const getCongViecChiTiet = createAsyncThunk(
  "getCongVietChiTiet",
  async (id) => {
    try {
      const request = await congViecService.layCongViecChiTiet(id);
      // console.log(request.data.content);
      return request.data.content;
    } catch ({ message }) {}
  }
);

export const getBinhLuanTheoCongViec = createAsyncThunk(
  "getBinhLuanTheoCongViec",
  async (id) => {
    try {
      const request = await congViecService.layBinhLuanTheoCongViec(id);
      console.log(request.data.content);
      return request.data.content;
    } catch ({ message }) {
      console.log(message);
    }
  }
);

export const binhLuan = createAsyncThunk("binhLuan", async (data) => {
  try {
    const request = await congViecService.binhLuan(data);
    console.log(request);
    return request.data.content;
  } catch ({ message }) {
    console.log(message);
  }
});
