import { createSlice } from "@reduxjs/toolkit";
import { userLocalStorage } from "../../services/localStorageService";
import { postDangKy, postDangNhap } from "../thunk/userThunk";
import { getDanhSachDaThue, getUserTheoID } from "../thunk/userThunk";
import {
  deleteUser,
  getUsersData,
  addUser,
  getUserById,
} from "../thunk/userThunk";

const initialState = {
  userDangNhap: userLocalStorage.get(),
  loadingUser: false,
  // userDangKy: null,
  users: [],
  tobeModifiedUser: null,
  loading: false,
  error: "",
  thueCongViec: [],
};

export const userSlice = createSlice({
  name: "userSlice",
  initialState,

  reducers: {
    layThongTinUserDangNhap: (state, action) => {
      state.userDangNhap = action.payload;
    },
  },

  extraReducers: (builder) => {
    // Slice 1
    builder.addCase(postDangNhap.pending, (state) => {
      state.loadingUser = true;
      state.userDangNhap = {};
    });
    builder.addCase(postDangNhap.fulfilled, (state, { payload }) => {
      state.loadingUser = false;
      state.userDangNhap = payload;
    });

    builder.addCase(getUsersData.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(getUsersData.fulfilled, (state, action) => {
      state.loading = false;
      state.users = action.payload;
      state.error = "";
    });

    builder.addCase(getUsersData.rejected, (state, action) => {
      state.loading = false;
      state.users = [];
      state.error = action.error.message;
    });

    builder.addCase(getUserById.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(getUserById.fulfilled, (state, action) => {
      console.log(action.payload);
      state.loading = false;
      state.tobeModifiedUser = action.payload;
    });

    builder.addCase(getUserById.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });

    builder.addCase(deleteUser.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(deleteUser.fulfilled, (state) => {
      state.loading = false;
    });

    builder.addCase(deleteUser.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload.message;
    });

    builder.addCase(addUser.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(addUser.fulfilled, (state) => {
      state.loading = false;
    });

    builder.addCase(addUser.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload.message;
    });

    builder.addCase(getUserTheoID.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(getUserTheoID.fulfilled, (state, action) => {
      state.loading = false;
      state.user = action.payload;
    });

    builder.addCase(getUserTheoID.rejected, (state, action) => {
      state.loading = false;
      state.error = action.message.value;
    });

    builder.addCase(getDanhSachDaThue.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(getDanhSachDaThue.fulfilled, (state, action) => {
      state.loading = false;
      state.thueCongViec = action.payload;
    });

    builder.addCase(getDanhSachDaThue.rejected, (state, action) => {
      state.loading = false;
      state.error = action.message.value;
    });
  },
});

export const { layThongTinUserDangNhap } = userSlice.actions;
export default userSlice.reducer;
