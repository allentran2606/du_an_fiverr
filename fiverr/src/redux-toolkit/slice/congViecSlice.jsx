import { createSlice } from "@reduxjs/toolkit";
import {
  getCongViecTheoChiTietLoai,
  getDanhSachCongViecTheoTen,
  getMenuLoaiCongViec,
  getCongViecChiTiet,
  getBinhLuanTheoCongViec,
  binhLuan,
} from "../thunk/congViecThunk";

const initialState = {
  danhSachCongViecTheoTen: [],
  menuLoaiCongViec: [],
  congViecTheoChiTiet: [],
  congViecChiTiet: [],
  loadingCongViec: false,
  binhLuanTheoCongViec: [],
};

export const congViecSlice = createSlice({
  name: "congViecSlice",
  initialState,

  reducers: {
    layDanhSachCongViec: (state, action) => {
      state.danhSachCongViecTheoTen = action.payload;
    },
    layMenuCongViec: (state, action) => {
      state.menuLoaiCongViec = action.payload;
    },
    layCongViecTheoChiTietLoai: (state, action) => {
      state.congViecTheoChiTiet = action.payload;
    },
  },

  extraReducers: (builder) => {
    // slice 1
    builder.addCase(getDanhSachCongViecTheoTen.pending, (state) => {
      state.loadingCongViec = true;
      state.danhSachCongViecTheoTen = [];
    });
    builder.addCase(
      getDanhSachCongViecTheoTen.fulfilled,
      (state, { payload }) => {
        state.loadingCongViec = false;
        state.danhSachCongViecTheoTen = payload;
      }
    );

    // slice 2
    builder.addCase(getMenuLoaiCongViec.pending, (state) => {
      state.loadingCongViec = true;
      state.menuLoaiCongViec = [];
    });
    builder.addCase(getMenuLoaiCongViec.fulfilled, (state, { payload }) => {
      state.loadingCongViec = false;
      state.menuLoaiCongViec = payload;
    });

    // slice 3
    builder.addCase(getCongViecTheoChiTietLoai.pending, (state) => {
      state.loadingCongViec = true;
      state.congViecTheoChiTiet = [];
    });
    builder.addCase(
      getCongViecTheoChiTietLoai.fulfilled,
      (state, { payload }) => {
        state.loadingCongViec = false;
        state.congViecTheoChiTiet = payload;
      }
    );

    builder.addCase(getCongViecChiTiet.pending, (state) => {
      state.loadingCongViec = true;
    });

    builder.addCase(getCongViecChiTiet.fulfilled, (state, action) => {
      state.loadingCongViec = false;
      state.congViecChiTiet = action.payload;
    });

    builder.addCase(getCongViecChiTiet.rejected, (state, action) => {
      state.loadingCongViec = false;
    });

    builder.addCase(getBinhLuanTheoCongViec.pending, (state) => {
      state.loadingCongViec = true;
    });

    builder.addCase(getBinhLuanTheoCongViec.fulfilled, (state, action) => {
      state.loadingCongViec = false;
      state.binhLuanTheoCongViec = action.payload;
    });

    builder.addCase(getBinhLuanTheoCongViec.rejected, (state, action) => {
      state.loadingCongViec = false;
    });

    builder.addCase(binhLuan.pending, (state) => {
      state.loadingCongViec = true;
    });

    builder.addCase(binhLuan.fulfilled, (state) => {
      state.loadingCongViec = false;
    });

    builder.addCase(binhLuan.rejected, (state, action) => {
      state.loadingCongViec = false;
    });
  },
});

export const {
  layDanhSachCongViec,
  layMenuCongViec,
  layCongViecTheoChiTietLoai,
} = congViecSlice.actions;

export default congViecSlice.reducer;
