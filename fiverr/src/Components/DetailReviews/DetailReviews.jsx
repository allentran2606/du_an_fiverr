import React from "react";
import { useSelector } from "react-redux";
import { Rate } from "antd";
import { Progress } from "antd";

const DetailReviews = () => {
  const [job] = useSelector((state) => state.congViecSlice.congViecChiTiet);

  return (
    <div className='xl:w-3/5 lg:w-3/5 md:w-4/5 p-5'>
      <div className='flex justify-between items-center'>
        <div className='flex items-center'>
          <h1>{job?.congViec.danhGia} Reviews</h1>
          <Rate disabled value={job?.congViec.saoCongViec} allowHalf />
        </div>
        <span>
          Sort By <span>Most Relevant</span>
        </span>
      </div>
      <div className='flex'>
        <ul className='w-1/2'>
          <li className='flex'>
            <span className='mr-2'>5⭐</span>
            <Progress
              percent={522}
              format={(number) => {
                return number * 5.22;
              }}
            />
          </li>
          <li className='flex'>
            <span className='mr-2'>4⭐</span>
            <Progress
              percent={2}
              format={(number) => {
                return number;
              }}
            />
          </li>
          <li className='flex'>
            <span className='mr-2'>3⭐</span>
            <Progress
              percent={0}
              format={(number) => {
                return number;
              }}
            />
          </li>
          <li className='flex'>
            <span className='mr-2'>2⭐</span>
            <Progress
              percent={0}
              format={(number) => {
                return number;
              }}
            />
          </li>
          <li className='flex'>
            <span className='mr-2'>1⭐</span>
            <Progress
              percent={0}
              format={(number) => {
                return number;
              }}
            />
          </li>
        </ul>
        <div className='w-1/2 text-left'>
          <h2 className='font-medium'>Ratting Breakdown</h2>
          <ul>
            <li>Seller communication level</li>
            <li>Recommend to a friend</li>
            <li>Service as</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default DetailReviews;
