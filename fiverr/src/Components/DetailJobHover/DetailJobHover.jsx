import React from "react";
import { NavLink } from "react-router-dom";

export default function DetailJobHover({ listChiTietViec }) {
  const renderContentChiTiet = (dsChiTietLoai) => {
    return dsChiTietLoai.map((value, key) => {
      return (
        <NavLink to={`/category`}>
          <li className="text-base text-gray-400 py-1 font-medium" key={key}>
            {value.tenChiTiet}
          </li>
        </NavLink>
      );
    });
  };

  const renderChiTietLoaiCongViec = () => {
    return listChiTietViec.map((item, index) => {
      return (
        <li className="" key={index}>
          <h3 className="font-bold text-lg mt-3 mb-1">{item.tenNhom}</h3>
          <ul>{renderContentChiTiet(item.dsChiTietLoai)}</ul>
        </li>
      );
    });
  };
  return <div>{renderChiTietLoaiCongViec()}</div>;
}
