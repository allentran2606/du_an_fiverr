import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getCongViecTheoChiTietLoai } from "../../redux-toolkit/thunk/congViecThunk";

export default function Explore({ param }) {
  const dispatch = useDispatch();
  const id = param.id;

  useEffect(() => {
    dispatch(getCongViecTheoChiTietLoai(+id));
  }, []);

  const listJobExplore = useSelector(
    (state) => state.congViecSlice.congViecTheoChiTiet
  );

  const listMenuJob = useSelector(
    (state) => state.congViecSlice.menuLoaiCongViec
  );

  // Bóc tách array từ từ ra (do các array lồng vào nhau khó xử lý)
  const listNhomChiTietLoai = [];
  if (listJobExplore !== undefined) {
    listJobExplore.map((item, index) => {
      listNhomChiTietLoai.push(item.dsNhomChiTietLoai);
    });
  }

  // Bóc tách array từ từ ra (do các array lồng vào nhau khó xử lý)
  const listChiTietLoai = [];
  listNhomChiTietLoai.map((value, pos) => {
    value.map((item) => {
      listChiTietLoai.push(item);
    });
  });

  const renderContent = () => {
    return listChiTietLoai.map((item, index) => {
      return (
        <div className="content-wrapper text-left" key={index}>
          <div className="image-wrapper">
            <img src={item.hinhAnh} alt="" className="rounded-md w-full h-48" />
          </div>
          <div className="content-list">
            <h3 className="text-xl mt-6 mb-3 font-bold">{item.tenNhom}</h3>
            <ul>
              {item.dsChiTietLoai.map((value, index) => {
                return (
                  <NavLink to={"/notfound"}>
                    <li
                      key={index}
                      className="text-lg py-2 text-gray-500 font-medium"
                    >
                      {value.tenChiTiet}
                    </li>
                  </NavLink>
                );
              })}
            </ul>
          </div>
        </div>
      );
    });
  };

  return (
    <div className="explore container mx-auto mt-14">
      <h2 className="font-bold text-2xl text-left">
        Explore Digital Marketing
      </h2>
      <div className="explore-wrapper grid gap-10 mt-6 lg:grid-cols-4 sm:grid-cols-3">
        {renderContent()}
      </div>
    </div>
  );
}
