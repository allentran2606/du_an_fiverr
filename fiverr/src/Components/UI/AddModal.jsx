import React from "react";
import AddForm from "../AdminForm/AddForm";
import { useState } from "react";
import { Button, Modal } from "antd";

const AddModal = () => {
  const [open, setOpen] = useState(false);

  const showModal = () => {
    setOpen(true);
  };

  const handleCancel = () => {
    setOpen(false);
  };

  const handleOk = () => {
    console.log("clicked");
  };

  return (
    <div>
      <Button className='bg-blue-500 text-white' onClick={showModal}>
        Thêm quản trị viên{" "}
      </Button>
      <Modal
        open={open}
        title='Thêm quản trị viên'
        onCancel={handleCancel}
        footer={[
          <Button key='back' onClick={handleCancel}>
            Cancel
          </Button>,
        ]}
      >
        <AddForm />
      </Modal>
    </div>
  );
};

export default AddModal;
