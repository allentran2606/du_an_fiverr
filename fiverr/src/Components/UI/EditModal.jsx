import React, { useEffect } from "react";
import { Button, Modal } from "antd";
import EditForm from "../AdminForm/EditForm";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { getUserById } from "../../redux-toolkit/thunk/userThunk";

const EditModal = ({ id }) => {
  console.log(id);
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserById(id));
  }, [id]);

  const showModal = () => {
    setOpen(true);
  };

  const handleCancel = () => {
    setOpen(false);
  };

  return (
    <div>
      <button
        className='text-white bg-yellow-500 hover:text-white hover:bg-yellow-700 rounded p-2'
        onClick={() => {
          console.log(id);
          showModal();
        }}
      >
        Edit
      </button>
      <div>
        <Modal
          open={open}
          title='Thay đổi thông tin nhân viên'
          onCancel={handleCancel}
          footer={[
            <Button key='back' onClick={handleCancel}>
              Cancel
            </Button>,
          ]}
        >
          <EditForm />
        </Modal>
      </div>
    </div>
  );
};

export default EditModal;
