import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button, Form, Input, Select, notification, Col } from "antd";
import { useDispatch } from "react-redux";
import { addUser } from "../../redux-toolkit/thunk/userThunk";

const AddForm = () => {
  const dispatch = useDispatch();

  const addHandler = (newUser) => {
    dispatch(addUser(newUser));

    setTimeout(() => {
      window.location.reload();
    }, 1000);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      birthday: "",
      phone: "",
      password: "",
      role: "ADMIN",
    },

    validationSchema: Yup.object({
      name: Yup.string()
        .min(2, "Tên không hợp lệ")
        .required("Tên không được để trống"),
      email: Yup.string().email().required("Email không được để trống"),
      phone: Yup.number()
        .min(9, "Số điện thoại không hợp lệ")
        .required("SĐT không được để trống"),
      birthday: Yup.date().required("Ngày sinh không được để trống"),
      password: Yup.string()
        .required("Mật khẩu không được để trống")
        .min(6, "Mật khẩu quá ngắn"),
    }),

    onSubmit: (values) => {
      console.log(values);
      addHandler(values);
    },
  });

  return (
    <div className='text-start'>
      <form onSubmit={formik.handleSubmit}>
        <label
          className={
            formik.touched.name && formik.errors.name ? "text-red-500" : ""
          }
        >
          {formik.touched.name && formik.errors.name
            ? formik.errors.name
            : "Name"}
        </label>
        <Input
          placeholder='Name'
          onBlur={formik.handleBlur}
          name='name'
          value={formik.values.name}
          onChange={formik.handleChange}
        />
        <label
          className={
            formik.touched.email && formik.errors.email ? "text-red-500" : ""
          }
        >
          {formik.touched.email && formik.errors.email
            ? formik.errors.email
            : "Email"}
        </label>
        <Input
          placeholder='Email'
          onBlur={formik.handleBlur}
          name='email'
          value={formik.values.email}
          onChange={formik.handleChange}
        />
        <label
          className={
            formik.touched.phone && formik.errors.phone ? "text-red-500" : ""
          }
        >
          {formik.touched.phone && formik.errors.phone
            ? formik.errors.phone
            : "Phone"}
        </label>
        <Input
          placeholder='Phone'
          onBlur={formik.handleBlur}
          name='phone'
          value={formik.values.phone}
          onChange={formik.handleChange}
        />
        <label
          className={
            formik.touched.birthday && formik.errors.birthday
              ? "text-red-500"
              : ""
          }
        >
          {formik.touched.birthday && formik.errors.birthday
            ? formik.errors.birthday
            : "Birthday"}
        </label>
        <Input
          placeholder='Birthday'
          type='date'
          onBlur={formik.handleBlur}
          name='birthday'
          value={formik.values.birthday}
          onChange={formik.handleChange}
        />
        <label
          className={
            formik.touched.password && formik.errors.password
              ? "text-red-500"
              : ""
          }
        >
          {formik.touched.password && formik.errors.password
            ? formik.errors.password
            : "Password"}
        </label>
        <Input
          placeholder='Password'
          type='password'
          onBlur={formik.handleBlur}
          name='password'
          value={formik.values.password}
          onChange={formik.handleChange}
        />
        <button
          type='submit'
          className='bg-blue-600 text-white rounded p-2 flex self-end'
        >
          Thêm
        </button>
      </form>
    </div>
  );
};

export default AddForm;
