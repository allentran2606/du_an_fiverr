import React from "react";
import { useFormik, Field, FormikProps, Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { Input, Select } from "antd";
import { updateUser } from "../../redux-toolkit/thunk/userThunk";

const EditForm = () => {
  const dispatch = useDispatch();
  const { id, name, email, phone, role, birthday } = useSelector(
    (state) => state.userSlice.tobeModifiedUser
  );

  const handleChange = (value) => {
    formik.values.role = value;
  };

  const editHandler = (user) => {
    dispatch(updateUser(user));

    setTimeout(() => {
      window.location.reload();
    }, 1000);
  };

  const formik = useFormik({
    initialValues: {
      id: id,
      name: name,
      email: email,
      birthday: birthday,
      phone: phone,
      role: role,
    },

    validationSchema: Yup.object({
      name: Yup.string()
        .min(2, "Tên không hợp lệ")
        .required("Tên không được để trống"),
      email: Yup.string().email().required("Email không được để trống"),
      phone: Yup.number()
        .min(9, "Số điện thoại không hợp lệ")
        .required("SĐT không được để trống"),
      birthday: Yup.date().required("Ngày sinh không được để trống"),
    }),

    onSubmit: (values) => {
      editHandler(values);
    },
  });

  return (
    <div className='text-start'>
      <form onSubmit={formik.handleSubmit}>
        <label
          className={
            formik.touched.name && formik.errors.name ? "text-red-500" : ""
          }
        >
          {formik.touched.name && formik.errors.name
            ? formik.errors.name
            : "Name"}
        </label>
        <Input
          placeholder='Name'
          onBlur={formik.handleBlur}
          name='name'
          value={formik.values.name}
          onChange={formik.handleChange}
        />
        <label
          className={
            formik.touched.email && formik.errors.email ? "text-red-500" : ""
          }
        >
          {formik.touched.email && formik.errors.email
            ? formik.errors.email
            : "Email"}
        </label>
        <Input
          placeholder='Email'
          onBlur={formik.handleBlur}
          name='email'
          value={formik.values.email}
          onChange={formik.handleChange}
        />
        <label
          className={
            formik.touched.phone && formik.errors.phone ? "text-red-500" : ""
          }
        >
          {formik.touched.phone && formik.errors.phone
            ? formik.errors.phone
            : "Phone"}
        </label>
        <Input
          placeholder='Phone'
          onBlur={formik.handleBlur}
          name='phone'
          value={formik.values.phone}
          onChange={formik.handleChange}
        />
        <label
          className={
            formik.touched.birthday && formik.errors.birthday
              ? "text-red-500"
              : ""
          }
        >
          {formik.touched.birthday && formik.errors.birthday
            ? formik.errors.birthday
            : "Birthday"}
        </label>
        <Input
          placeholder='Birthday'
          type='date'
          onBlur={formik.handleBlur}
          name='birthday'
          value={formik.values.birthday}
          onChange={formik.handleChange}
        />
        <Select
          onBlur={formik.handleBlur}
          name='role'
          onChange={handleChange}
          defaultValue={role}
          options={[
            {
              value: "ADMIN",
              label: "Admin",
            },
            {
              value: "USER",
              label: "User",
            },
          ]}
        />
        <button
          type='submit'
          className='bg-blue-600 text-white rounded p-2 flex self-end'
        >
          Sửa
        </button>
      </form>
    </div>
  );
};

export default EditForm;
