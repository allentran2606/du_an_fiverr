import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Input } from "antd";

const AdminSearchBar = () => {
  const formikSearch = useFormik({
    initialValues: {
      input: "",
    },

    validationSchema: Yup.object({
      input: Yup.string().required("Không được để trống"),
    }),

    onSubmit: (values) => {
      console.log(values);
    },
  });
  return (
    <div>
      <form onSubmit={formikSearch.handleSubmit} className='flex'>
        <Input
          name='input'
          onChange={formikSearch.handleChange}
          value={formikSearch.values.input}
          placeholder='Nhập vào tài khoản hoặc họ tên người dùng'
        />
        <button
          className='hover:text-white hover:bg-green-700 text-white bg-green-500 rounded p-2'
          type='submit'
        >
          search
        </button>
      </form>
    </div>
  );
};

export default AdminSearchBar;
