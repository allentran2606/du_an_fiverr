import { useState } from "react";
import "./Tabs";
import { useSelector } from "react-redux";
import "./Tabs.css";
import UserNav from "../UserNav/UserNav";
import { Navigate, useNavigate } from "react-router-dom";
import { userService } from "../../services/userService";
import moment from "moment/moment";

export default function Tabs() {
  const [toggleState, setToggleState] = useState(1);
  const navigate = useNavigate();
  const userDangNhap = useSelector((state) => state.userSlice.userDangNhap);

  const [job] = useSelector((state) => state.congViecSlice.congViecChiTiet);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  return (
    <div className='Tabs__container'>
      <div className='bloc-tabs'>
        <button
          className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(1)}
        >
          Basic
        </button>
        <button
          className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(2)}
        >
          Standard
        </button>
        <button
          className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(3)}
        >
          Premium
        </button>
      </div>

      <div className='content-tabs'>
        <div
          className={toggleState === 1 ? "content  active-content" : "content"}
        >
          <div className='content-header flex justify-between px-5'>
            <h2>Basic</h2>
            <h2 className='text-green-500'>${job?.congViec.giaTien}</h2>
          </div>
          <p className='flex-col'>{job?.congViec.moTaNgan}</p>
        </div>

        <div
          className={toggleState === 2 ? "content  active-content" : "content"}
        >
          <div className='content-header flex justify-between px-5'>
            <h2>Standard</h2>
            <h2 className='text-green-500'>${job?.congViec.giaTien}</h2>
          </div>
          <p>{job?.congViec.moTaNgan}</p>
        </div>

        <div
          className={toggleState === 3 ? "content  active-content" : "content"}
        >
          <div className='content-header flex justify-between px-5'>
            <h2>Premium</h2>
            <h2 className='text-green-500'>${job?.congViec.giaTien}</h2>
          </div>
          <p>{job?.congViec.moTaNgan}</p>
        </div>
      </div>

      <button
        onClick={() => {
          if (!userDangNhap) {
            navigate("/login");
          } else {
            let data = {
              id: 0,
              maCongViec: job?.id,
              maNguoiThue: userDangNhap.user?.id,
              ngayThue: moment().format("DD/MM/YYYY"),
              hoanThanh: true,
            };
            console.log(data);
            userService
              .thueCongViec(data)
              .then((result) => {
                console.log(result);
              })
              .catch((err) => {
                console.log(err);
              });
          }
        }}
        className='mx-5 p-3 bg-green-500 text-white'
      >
        Continue({job?.congViec.giaTien}$)
      </button>
      <a href='##' className='p-3  text-green-500'>
        Compare Packages
      </a>
    </div>
  );
}
