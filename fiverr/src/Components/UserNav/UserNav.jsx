import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalStorage } from "../../services/localStorageService";

export default function UserNav() {
  const userDangNhap = useSelector((state) => state.userSlice.userDangNhap);

  const handleLogout = () => {
    userLocalStorage.remove();
    window.location.href = "/login";
  };

  const renderAdminTab = () => {
    if (userDangNhap.user.role === "ADMIN") {
      return (
        <NavLink to="/admin">
          <button className="text-white bg-green-400 border border-green-400 hover:bg-green-600 hover:border-green-600 transition-all font-medium rounded-md text-base px-5 py-2 mr-2 logout-button">
            Admin dashboard
          </button>
        </NavLink>
      );
    } else {
      return null;
    }
  };

  const renderUserNav = () => {
    if (userDangNhap) {
      return (
        <>
          <span className="text-white font-medium mr-4">
            Welcome {userDangNhap.user.name}!
          </span>
          {renderAdminTab()}
          <button
            className="text-white bg-transparent border border-white hover:bg-green-400 hover:border-green-400 transition-all font-medium rounded-md text-base px-5 py-2 mr-2 logout-button"
            onClick={handleLogout}
          >
            Log out
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink
            to={"/"}
            className="text-white text-base font-medium rounded-lg px-4 lg:px-5 py-2 lg:py-2.5"
          >
            Become a seller
          </NavLink>
          <NavLink
            to={"/login"}
            className="text-white font-medium rounded-lg text-base px-4 lg:px-5 py-2 lg:py-2.5 mr-2"
          >
            Log in
          </NavLink>
          <NavLink
            to={"/register"}
            className="text-white bg-transparent border border-white hover:bg-green-400 hover:border-green-400 transition-all font-medium rounded-md text-base px-5 py-2 mr-2 join-button"
          >
            Join
          </NavLink>
        </>
      );
    }
  };

  return <div>{renderUserNav()}</div>;
}
