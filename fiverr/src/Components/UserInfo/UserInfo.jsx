import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDanhSachDaThue } from "../../redux-toolkit/thunk/userThunk";

const UserInfo = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.userSlice.user);
  const jobs = useSelector((state) => state.userSlice.danhSachDaThue);

  useEffect(() => {
    dispatch(getDanhSachDaThue());
  });

  console.log(jobs);

  return (
    <div>
      <div className='flex flex-col'>
        <img src={user?.avatar} alt='true' />
        <div>
          <h2>{user?.name}</h2>
        </div>
        <h2>{user?.email}</h2>
        <h2>{user?.phone}</h2>
      </div>
      <div></div>
    </div>
  );
};

export default UserInfo;
