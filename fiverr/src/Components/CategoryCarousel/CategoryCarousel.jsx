import React from "react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import { NavLink } from "react-router-dom";

export default function CategoryCarousel() {
  return (
    <div className="container mx-auto mt-12">
      <div className="category-carousel">
        <Swiper
          slidesPerView={5}
          spaceBetween={30}
          slidesPerGroup={5}
          navigation={true}
          modules={[Navigation]}
          className="mySwiper relative"
        >
          <div className="category-carousel-navigation absolute top-0 z-10">
            <h2 className="font-bold text-2xl text-left">
              Most Popular in Digital Marketing
            </h2>
          </div>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101614/Logo%20design_2x.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">Minimalist Logo Design</span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101618/Architecture%20_%20Interior%20Design_2x.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">
                    Architecture & Interior Design
                  </span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/55b9d6349057bb9fe177ea57e2d92f30-1670570507381/Web%20Design.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">Website Design</span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101633/Illustration_2x.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">Illustration</span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101624/Photoshop%20Editing_2x.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">Image Editing</span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101623/T-Shirts%20_%20Merchandise_2x.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">T-Shirts & Merchandise</span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/5987755afeb2d9ea01871fdee90a9a05-1670570470543/Product%20_%20industrial%20design.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">
                    Industrial & Product Design
                  </span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101621/Social%20Media%20Design_2x.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">Social Media Design</span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
          <SwiperSlide className="swiper-slide mt-14">
            <div>
              <NavLink>
                <div className="flex items-center bg-gray-300 rounded-xl border-gray-100 p-3 w-fit">
                  <img
                    src="https://fiverr-res.cloudinary.com/f_auto,q_auto/v1/attachments/generic_asset/asset/fc6c7b8c1d155625e7878252a09c4437-1653222039380/Nft%20Art%20%281%29.png"
                    alt="true"
                    className="mr-3 w-12 h-12"
                  />
                  <span className="font-medium">NFT Art</span>
                  <span
                    className=" ml-2"
                    style={{ width: 16, height: 16 }}
                    aria-hidden="true"
                  >
                    <svg
                      width={16}
                      height={16}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z" />
                    </svg>
                  </span>
                </div>
              </NavLink>
            </div>
          </SwiperSlide>
        </Swiper>
      </div>
    </div>
  );
}
