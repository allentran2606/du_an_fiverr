import React from "react";

export default function SubFooter() {
  return (
    <div className="sub-footer container mx-auto px-8 mt-14 mb-20">
      <h2 className="font-bold text-2xl">
        Services Related To Digital Marketing
      </h2>
      <ul className="flex items-center justify-center flex-wrap mt-12">
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          SEO Services
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Marketing Strategy
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Social Media Marketing
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Affiliate Marketing
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          eCommerce Marketing
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Influencer Marketing
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Music Promotion
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Search Engine Marketing (SEM)
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Email Marketing
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Video Marketing
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Shoutouts & Promotions
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Youtube Marketing
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Social Content
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Web Analytics Services
        </li>
        <li className="px-7 text-lg text-gray-500 font-medium mb-4">
          Spotify Music Promotion
        </li>
      </ul>
    </div>
  );
}
