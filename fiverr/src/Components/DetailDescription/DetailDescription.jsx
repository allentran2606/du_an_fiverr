import React from "react";
import { useSelector } from "react-redux";
import { Rate } from "antd";

const DetailDescription = () => {
  const [job] = useSelector((state) => state.congViecSlice.congViecChiTiet);

  return (
    <div className='p-5 xl:w-3/5 lg:w-3/5 md:w-4/5'>
      <div className='text-start'>
        <h1>About This Gig</h1>
        <p>{job?.congViec.moTa}</p>
      </div>
      <div className='flex my-5'>
        <img
          className='w-32 h-32 rounded-full mr-2 '
          src={job?.avatar}
          alt='true'
        />
        <div className='flex-col text-start'>
          <h2>{job?.tenNguoiTao}</h2>
          <span>Web Developer</span>
          <div className='flex'>
            <Rate disabled value={job?.congViec.saoCongViec} allowHalf />
            <h2 className='self-center'>({job?.congViec.danhGia})</h2>
          </div>
          <button className='plans-btn'>Contact Me</button>
        </div>
      </div>
    </div>
  );
};

export default DetailDescription;
