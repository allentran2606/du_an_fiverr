import React from "react";
import { useSelector } from "react-redux";
import { Rate } from "antd";

const DetailHeader = () => {
  const [job] = useSelector((state) => state.congViecSlice.congViecChiTiet);

  return (
    <div className='p-5'>
      <div className='border-b p-3 flex flex-col items-start'>
        <h1 className='font-medium text-2xl pb-3'>
          {job?.congViec.tenCongViec}
        </h1>
        <div className='flex items-center'>
          <img className='w-8 rounded-full mr-2' src={job?.avatar} alt='true' />
          <h2 className='font-medium mr-2'>{job?.tenNguoiTao}</h2>
          <span className='text-yellow-500 mr-2'>Top Rated Seller</span>
          <div className='flex border-x mr-2'>
            <Rate disabled value={job?.congViec.saoCongViec} allowHalf />
            <h2 className='self-center'>({job?.congViec.danhGia})</h2>
          </div>
          <span className='text-slate-400'>4 Orders in Queue</span>
        </div>
      </div>
    </div>
  );
};

export default DetailHeader;
