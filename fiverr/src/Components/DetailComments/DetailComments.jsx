import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useFormik } from "formik";
import { binhLuan } from "../../redux-toolkit/thunk/congViecThunk";
import { useNavigate } from "react-router-dom";
import { userService } from "../../services/userService";
import moment from "moment";
import { congViecService } from "../../services/congViecService";

const DetailComments = () => {
  const navigate = useNavigate();
  const comments = useSelector(
    (state) => state.congViecSlice.binhLuanTheoCongViec
  );
  const userDangNhap = useSelector((state) => state.userSlice.userDangNhap);
  const dispatch = useDispatch();
  const [job] = useSelector((state) => state.congViecSlice.congViecChiTiet);

  const renderComments = () => {
    return comments?.map(
      ({ avatar, tenNguoiBinhLuan, noiDung, saoBinhLuan, ngayBinhLuan }) => {
        return (
          <div className="text-start">
            <div className="flex">
              <img
                className="w-16 h-16 rounded-full"
                src={avatar}
                alt="avatar"
              />
              <h2>{tenNguoiBinhLuan}</h2>
              <h2>{saoBinhLuan}⭐</h2>
            </div>
            <div>
              <p>{noiDung}</p>
              <h2>{ngayBinhLuan}</h2>
            </div>
          </div>
        );
      }
    );
  };

  const formikComments = useFormik({
    initialValues: {
      // id: 0,
      // maCongViec: "",
      // ngayBinhLuan: "",
      // maNguoiBinhLuan: "",
      // saoBinhLuan: 5,
      noiDung: "",
    },

    onSubmit: (values) => {
      if (!userDangNhap) {
        navigate("/login");
      } else {
        const data = {
          id: 0,
          maCongViec: job?.id,
          ngayBinhLuan: moment().format("DD/MM/YY"),
          maNguoiBinhLuan: 0,
          saoBinhLuan: 4,
          noiDung: values.noiDung,
        };
        congViecService
          .binhLuan(data)
          .then((result) => {
            console.log(result);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    },
  });

  return (
    <div className="detail-comment">
      <div className="border-b-2 p-5 xl:text-start lg:text-start md:text-start sm:text-center">
        <h1 className="font-medium">Filters</h1>
        <h2>
          industry <span>All Industries</span>
        </h2>
      </div>
      <div>{renderComments()}</div>
      <div>
        <form onSubmit={formikComments.handleSubmit}>
          <input
            className="border"
            rows="5"
            cols="100"
            name="input"
            value={formikComments.values.name}
            onChange={formikComments.handleChange}
            onBlur={formikComments.handleBlur}
          />
          <button
            className="bg-red-500 px-2 py-1 rounded text-white"
            type="submit"
            onClick={() => {
              console.log("clicked");
            }}
          >
            Bình luận
          </button>
        </form>
      </div>
    </div>
  );
};

export default DetailComments;
