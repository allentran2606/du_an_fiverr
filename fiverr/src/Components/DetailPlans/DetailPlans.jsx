import { Card } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import Tabs from "../Tabs/Tabs";

const DetailPlans = () => {
  return (
    <div>
      <Tabs />
      <Card style={{ width: "500px", border: "none" }}>
        <h2>Do you have any special requirements?</h2>
        <button className='plans-btn'>Get a quote</button>
      </Card>
    </div>
  );
};

export default DetailPlans;
