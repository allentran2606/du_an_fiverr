import React from "react";
import { NavLink } from "react-router-dom";
import "./Header.css";
import CategoryMenu from "../CategoryMenu/CategoryMenu";
import UserNav from "../UserNav/UserNav";

export default function Header() {
  window.addEventListener("scroll", function () {
    let header = document.querySelector("header");
    header.classList.toggle("sticky", window.scrollY > 0);
  });

  return (
    <header className="header fixed top-0 left-0 w-full transition-all z-10">
      <nav className="px-4 py-4 w-full">
        <div className="flex flex-wrap justify-start items-center mx-auto container">
          {/* Logo */}
          <NavLink to={"/"} className="flex items-center logo">
            <svg
              width="89"
              height="27"
              viewBox="0 0 89 27"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g fill="#fff" className="fiverr-text">
                <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path>
              </g>
              <g fill="#1dbf73">
                <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path>
              </g>
            </svg>
          </NavLink>

          {/* Login button */}
          <div className="flex items-center lg:order-2 ml-auto navigations">
            {/* <NavLink
              to={"/"}
              className="text-white text-base font-medium rounded-lg px-4 lg:px-5 py-2 lg:py-2.5"
            >
              Become a seller
            </NavLink>
             <NavLink
              to={"/login"}
              className="text-white font-medium rounded-lg text-base px-4 lg:px-5 py-2 lg:py-2.5 mr-2"
            >
              Log in
            </NavLink>
            <NavLink
              to={"/register"}
              className="text-white bg-transparent border border-white hover:bg-green-400 hover:border-green-400 transition-all font-medium rounded-md text-base px-5 py-2 mr-2 join-button"
            >
              Join
            </NavLink> */}
            <UserNav />
          </div>
        </div>
      </nav>
      <div className="hidden category-menu-header">
        <CategoryMenu />
      </div>
    </header>
  );
}
