import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { getDanhSachCongViecTheoTen } from "../../redux-toolkit/thunk/congViecThunk";
import DataResult from "./DataResult";
import { Input, message } from "antd";
import { useNavigate } from "react-router-dom";
const { Search } = Input;

export default function SearchBar() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // setState từ đang gõ vào input
  const [searchWord, setSearchWord] = useState("");

  // Filter khi gõ sẽ suggest ra tên công việc
  const [filteredInput, setFilteredInput] = useState([]);

  // Call api (lấy danh sách công việc theo tên)
  const fetchDanhSachCongViec = (nextValue) => {
    dispatch(getDanhSachCongViecTheoTen(nextValue));
  };

  // Debounce Search
  // const debounceSearch = useRef(
  //   debounce((nextValue) => fetchDanhSachCongViec(nextValue), 300)
  // ).current;

  // Lấy res (ở đây là danh sách công việc theo tên) từ api trả về
  // const jobData = useSelector(
  //   (state) => state.congViecSlice.danhSachCongViecTheoTen
  // );

  // Xử lý form input (do chưa hiểu rõ debounce search nên còn lỗi, xin phép bản thân comment lại để làm sau hihi :v)
  // const handleOnChange = (e) => {
  //   const { value } = e.target;
  //   setSearchWord(value);
  //   debounceSearch(value);
  //   if (value !== "") {
  //     const newFilter = jobData.filter((item) => {
  //       return item.tenChiTietLoai.toLowerCase().includes(value.toLowerCase());
  //     });
  //     setFilteredInput(newFilter);
  //   }
  // };

  const onSearch = (value) => {
    console.log(value);
    if (value !== "") {
      setSearchWord(value);
      fetchDanhSachCongViec(value);
      setTimeout(() => {
        navigate(`/categorytype`);
      }, 1000);
    } else {
      message.error("Please type something!");
    }
  };

  return (
    <div className=" overflow-x-hidden">
      <h1 className="text-white text-left text-5xl font-semibold mb-6">
        <span>
          Find the perfect <i>freelance</i> <br></br> services for your business
        </span>
      </h1>
      <div className="search-bar">
        <Search
          placeholder="Input search text"
          allowClear
          enterButton="Search"
          size="large"
          // onChange={handleOnChange}
          onSearch={onSearch}
          className="search-form"
        />
        {/* <DataResult filteredInput={filteredInput} /> */}
      </div>
      <div className="popular text-white flex items-center mt-6">
        Popular:
        <ul className="flex items-center ml-3">
          <li className="rounded-full border border-white px-3 py-0.5 mr-3">
            <a
              href="/categories/graphics-design/website-design?source=hplo_search_tag&pos=1&name=website-design"
              className="text-body-2"
            >
              Website Design
            </a>
          </li>
          <li className="rounded-full border border-white px-3 py-0.5 mr-3">
            <a
              href="/categories/programming-tech/wordpress-services?source=hplo_search_tag&pos=2&name=wordpress-services"
              className="text-body-2"
            >
              WordPress
            </a>
          </li>
          <li className="rounded-full border border-white px-3 py-0.5 mr-3">
            <a
              href="/categories/graphics-design/creative-logo-design?source=hplo_search_tag&pos=3&name=creative-logo-design"
              className="text-body-2"
            >
              Logo Design
            </a>
          </li>
          <li className="rounded-full border border-white px-3 py-0.5 mr-3">
            <a
              href="/cp/ai-services?source=hplo_search_tag&pos=4&name=ai-services"
              className="text-body-2"
            >
              AI Services
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
