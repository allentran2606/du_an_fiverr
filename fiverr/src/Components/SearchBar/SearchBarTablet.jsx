import { debounce } from "lodash";
import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDanhSachCongViecTheoTen } from "../../redux-toolkit/thunk/congViecThunk";
import DataResult from "./DataResult";
import { Input, message } from "antd";
import { useNavigate } from "react-router-dom";
const { Search } = Input;

export default function SearchBarTablet() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // setState từ đang gõ vào input
  const [searchWord, setSearchWord] = useState("");

  // Call api (lấy danh sách công việc theo tên)
  const fetchDanhSachCongViec = (nextValue) => {
    dispatch(getDanhSachCongViecTheoTen(nextValue));
  };

  const onSearch = (value) => {
    console.log(value);
    if (value !== "") {
      setSearchWord(value);
      fetchDanhSachCongViec(value);
      setTimeout(() => {
        navigate(`/categorytype`);
      }, 1000);
    } else {
      message.error("Please type something!");
    }
  };

  return (
    <div className="flex-shrink">
      <h1 className="text-white text-left lg:text-5xl md:text-4xl sm:text-4xl font-semibold mb-6">
        <span>
          Find the perfect <i>freelance</i> <br></br> services for your business
        </span>
      </h1>
      <div className="search-bar block">
        <Search
          placeholder="Input search text"
          allowClear
          enterButton="Search"
          size="large"
          onSearch={onSearch}
          className="search-form"
        />
        {/* <DataResult filteredInput={filteredInput} /> */}
      </div>
    </div>
  );
}
