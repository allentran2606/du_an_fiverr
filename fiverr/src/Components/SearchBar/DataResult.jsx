import React from "react";
import { NavLink } from "react-router-dom";

export default function DataResult({ filteredInput }) {
  return (
    <div>
      {filteredInput.length !== 0 && (
        <div className="data-result">
          {filteredInput.map((item, index) => {
            return (
              <NavLink key={index} className="data-result-item">
                <p className="text-left pl-11 pt-1">{item.tenChiTietLoai}</p>
              </NavLink>
            );
          })}
        </div>
      )}
    </div>
  );
}
