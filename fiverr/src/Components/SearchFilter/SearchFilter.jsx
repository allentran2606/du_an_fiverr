import React from "react";
import "./SearchFilter.css";
import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Space } from "antd";

export default function SearchFilter() {
  const handleOnClick = () => {
    document
      .getElementsByClassName("accordion-button")[0]
      .classList.toggle("is-open");
    document
      .getElementsByClassName("subcategories-list")[0]
      .classList.toggle("is-open");
  };

  const handleOnClickGroup = () => {
    document
      .getElementsByClassName("floating-menu")[0]
      .classList.toggle("open");
  };

  const items = [
    {
      label: <a href="https://www.antgroup.com">1st menu item</a>,
      key: "0",
    },
    {
      label: <a href="https://www.aliyun.com">2nd menu item</a>,
      key: "1",
    },
    {
      type: "divider",
    },
    {
      label: "3rd menu item",
      key: "3",
    },
  ];

  return (
    <div className="container mx-auto">
      <div className="result-for">
        <div className="search-header pt-8">
          <span className="title text-4xl font-bold">Results for "html"</span>
        </div>
      </div>

      <div id="topbar" className="sticky-wrapper">
        <div className="shadow-effect">
          <div className="floating-top-bar flex flex-wrap items-center justify-between">
            <div className="top-filters">
              {/* Button 1 */}
              <div className="button-1 border border-gray-300 py-1.5 px-3 font-medium cursor-pointer rounded-md mr-3">
                <Dropdown
                  menu={{
                    items,
                  }}
                  trigger={["click"]}
                >
                  <a onClick={(e) => e.preventDefault()}>
                    <Space>
                      Category
                      <DownOutlined />
                    </Space>
                  </a>
                </Dropdown>
              </div>

              {/* Button 2 */}
              <div className="button-2 border border-gray-300 py-1.5 px-3 font-medium cursor-pointer rounded-md mr-3">
                <Dropdown
                  menu={{
                    items,
                  }}
                  trigger={["click"]}
                >
                  <a onClick={(e) => e.preventDefault()}>
                    <Space>
                      Service Options
                      <DownOutlined />
                    </Space>
                  </a>
                </Dropdown>
              </div>

              {/* Button 3 */}
              <div className="button-3 border border-gray-300 py-1.5 px-3 font-medium cursor-pointer rounded-md mr-3">
                <Dropdown
                  menu={{
                    items,
                  }}
                  trigger={["click"]}
                >
                  <a onClick={(e) => e.preventDefault()}>
                    <Space>
                      Seller Details
                      <DownOutlined />
                    </Space>
                  </a>
                </Dropdown>
              </div>

              {/* Button 4 */}
              <div className="button-4 border border-gray-300 py-1.5 px-3 font-medium cursor-pointer rounded-md mr-3">
                <Dropdown
                  menu={{
                    items,
                  }}
                  trigger={["click"]}
                >
                  <a onClick={(e) => e.preventDefault()}>
                    <Space>
                      Budget
                      <DownOutlined />
                    </Space>
                  </a>
                </Dropdown>
              </div>

              {/* Button 5 */}
              <div className="button-5 border border-gray-300 py-1.5 px-3 font-medium cursor-pointer rounded-md">
                <Dropdown
                  menu={{
                    items,
                  }}
                  trigger={["click"]}
                >
                  <a onClick={(e) => e.preventDefault()}>
                    <Space>
                      Delivery Time
                      <DownOutlined />
                    </Space>
                  </a>
                </Dropdown>
              </div>
            </div>

            {/* Toggle */}
            <div className="togglers-wrapper flex flex-items-baseline mt-4 space-x-3">
              <label
                for="Toggle1"
                className="inline-flex items-center space-x-4 cursor-pointer"
              >
                <span className="relative">
                  <input id="Toggle1" type="checkbox" className="hidden peer" />
                  <div className="w-10 h-6 rounded-full shadow-inner bg-gray-300 peer-checked:bg-green-400"></div>
                  <div className="absolute inset-y-0 left-0 w-4 h-4 m-1 rounded-full shadow peer-checked:right-0 peer-checked:left-auto dark:bg-white"></div>
                </span>
                <span className="text-left font-medium text-gray-500">
                  Pro services
                </span>
              </label>
              <label
                for="Toggle2"
                className="inline-flex items-center space-x-4 cursor-pointer"
              >
                <span className="relative">
                  <input id="Toggle2" type="checkbox" className="hidden peer" />
                  <div className="w-10 h-6 rounded-full shadow-inner bg-gray-300 peer-checked:bg-green-400"></div>
                  <div className="absolute inset-y-0 left-0 w-4 h-4 m-1 rounded-full shadow peer-checked:right-0 peer-checked:left-auto dark:bg-white"></div>
                </span>
                <span className="text-left font-medium text-gray-500">
                  Local sellers
                </span>
              </label>
              <label
                for="Toggle3"
                className="inline-flex items-center space-x-4 cursor-pointer"
              >
                <span className="relative">
                  <input id="Toggle3" type="checkbox" className="hidden peer" />
                  <div className="w-10 h-6 rounded-full shadow-inner bg-gray-300 peer-checked:bg-green-400"></div>
                  <div className="absolute inset-y-0 left-0 w-4 h-4 m-1 rounded-full shadow peer-checked:right-0 peer-checked:left-auto dark:bg-white"></div>
                </span>
                <span className="text-left font-medium text-gray-500">
                  Online Sellers
                </span>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
