import React from "react";
import { useSelector } from "react-redux";
import { Swiper, SwiperSlide } from "swiper/react";
import { Thumbs, Navigation, FreeMode } from "swiper";
import { useState } from "react";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/navigation";
import "swiper/css/thumbs";
import "./DetailGallery.css";

const DetailGallery = () => {
  const [job] = useSelector((state) => state.congViecSlice.congViecChiTiet);
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  return (
    <div className="detail-gallery w-2/5">
      <Swiper
        onSlideChange={() => console.log("slide change")}
        watchSlidesProgress
        spaceBetween={10}
        navigation={true}
        thumbs={{ swiper: thumbsSwiper }}
        modules={[FreeMode, Navigation, Thumbs]}
        className="mySwiper2"
      >
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
      </Swiper>
      <Swiper
        onSwiper={setThumbsSwiper}
        spaceBetween={10}
        slidesPerView={4}
        freeMode={true}
        watchSlidesProgress={true}
        modules={[FreeMode, Navigation, Thumbs]}
        className="mySwiper"
      >
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={job?.congViec.hinhAnh} alt="true" />
        </SwiperSlide>
      </Swiper>
    </div>
  );
};

export default DetailGallery;
