import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./HOC/Layout";
import CategoryPage from "./Pages/CategoryPage/CategoryPage";
import CategoryTypePage from "./Pages/CategoryTypePage/CategoryTypePage";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import AdminPage from "./Pages/AdminPage/AdminPage";
import DetailPage from "./Pages/DetailPage/DetailPage";

function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <Routes>
          <Route
            path='/'
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route path='/login' element={<LoginPage />} />
          <Route path='/register' element={<RegisterPage />} />
          <Route path='/category/:id' element={<CategoryPage />} />
          <Route path='/categorytype' element={<CategoryTypePage />} />
          <Route path='/login' element={<LoginPage />} />
          <Route path='/admin' element={<AdminPage />} />
          <Route
            path='/detail'
            element={
              <Layout>
                <DetailPage />
              </Layout>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
