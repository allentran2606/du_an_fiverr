import React from "react";
import Carousel from "../../Components/Carousel/Carousel";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import Marketplace from "../../Components/Marketplace/Marketplace";
import Selling from "../../Components/Selling/Selling";
import ServiceCarousel from "../../Components/ServicesCarousel/ServiceCarousel";
import Testimonials from "../../Components/Testimonials/Testimonials";
import TrustedPartner from "../../Components/TrustedPartner/TrustedPartner";

export default function HomePage() {
  return (
    <div>
      <Carousel />
      <TrustedPartner />
      <ServiceCarousel />
      <Selling />
      <Testimonials />
      <Marketplace />
    </div>
  );
}
