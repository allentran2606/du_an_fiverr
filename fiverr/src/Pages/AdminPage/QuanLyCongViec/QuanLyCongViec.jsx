import React, { useEffect } from "react";
import { useState } from "react";
import { congViecService } from "../../../services/congViecService";
import { Table, Space } from "antd";

const QuanLyLoaiCongViec = () => {
  const [loaiCongViec, setLoaiCongViec] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const request = await congViecService.layLoaiCongViec();
        setLoaiCongViec(request.data.content);
        return request;
      } catch (error) {
        alert(error);
      }
    };
    fetchData();
  }, []);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Tên công việc",
      dataIndex: "tenLoaiCongViec",
      key: "tenLoaiCongViec",
    },
    {
      title: "Action",
      key: "action",
      render: () => (
        <Space size='middle'>
          <button className='hover:text-white hover:bg-slate-700 text-white bg-slate-500 rounded p-2'>
            Xem thông tin chi tiết
          </button>
          <button className='text-white bg-red-600 hover:text-white hover:bg-red-700 rounded p-2'>
            Delete
          </button>
          <button className='text-white bg-yellow-500 hover:text-white hover:bg-yellow-700 rounded p-2'>
            Sửa
          </button>
        </Space>
      ),
    },
  ];

  return (
    <div>
      <Table dataSource={loaiCongViec} columns={columns} />
    </div>
  );
};

export default QuanLyLoaiCongViec;
