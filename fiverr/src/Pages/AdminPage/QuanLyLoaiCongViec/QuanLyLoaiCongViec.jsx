import React, { useEffect } from "react";
import { useState } from "react";
import { congViecService } from "../../../services/congViecService";
import { Table, Space } from "antd";

const QuanLyCongViec = () => {
  const [congViec, setCongViec] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const request = await congViecService.layCongViec();
        setCongViec(request.data.content);
        return request;
      } catch (error) {
        alert(error);
      }
    };
    fetchData();
  }, []);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Tên công việc",
      dataIndex: "tenCongViec",
      key: "tenCongViec",
    },
    {
      title: "Hình Ảnh",
      key: "hinhAnh",
      dataIndex: "hinhAnh",
      width: 150,
      maxWidth: 150,
      render: (imgUrl) => {
        return <img alt='imgUrl' src={`${imgUrl}`} />;
      },
    },
    {
      title: "Đánh Giá",
      dataIndex: "danhGia",
      key: "danhGia",
    },
    {
      title: "Giá Tiền",
      dataIndex: "giaTien",
      key: "giaTien",
    },
    {
      title: "Action",
      key: "action",
      render: () => (
        <Space size='middle'>
          <button className='hover:text-white hover:bg-slate-700 text-white bg-slate-500 rounded p-2'>
            Xem thông tin chi tiết
          </button>
          <button className='text-white bg-red-600 hover:text-white hover:bg-red-700 rounded p-2'>
            Delete
          </button>
          <button className='text-white bg-yellow-500 hover:text-white hover:bg-yellow-700 rounded p-2'>
            Sửa
          </button>
        </Space>
      ),
    },
  ];

  return (
    <div>
      <Table columns={columns} dataSource={congViec} />
    </div>
  );
};

export default QuanLyCongViec;
