import React, { useState } from "react";
import { useEffect } from "react";
import { Table, Space, Button, Input, Modal, Tag, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  getUsersData,
  deleteUser,
} from "../../../redux-toolkit/thunk/userThunk";
import AddModal from "../../../Components/UI/AddModal";
import EditModal from "../../../Components/UI/EditModal";
import AdminSearchBar from "../../../Components/AdminForm/AdminSearchBar";
import { getUserById } from "../../../redux-toolkit/thunk/userThunk";

const QuanLyNguoiDung = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.userSlice.users);

  useEffect(() => {
    dispatch(getUsersData());
  }, [dispatch]);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Birthday",
      dataIndex: "birthday",
      key: "birthday",
    },
    {
      title: "Role",
      dataIndex: "role",
      key: "role",
      render: (role) => {
        return role.toLowerCase() === "admin" ? (
          <Tag color='blue'>{role}</Tag>
        ) : (
          <Tag>{role}</Tag>
        );
      },
    },
    {
      title: "Action",
      key: "action",
      dataIndex: "id",
      render: (id, record) => (
        <Space size='middle'>
          <button
            type='button'
            onClick={() => {
              deleteHandler(record.id);
            }}
            className='text-white bg-red-600 hover:text-white hover:bg-red-700 rounded p-2'
          >
            Delete
          </button>
          <EditModal id={id} />
        </Space>
      ),
    },
  ];

  const deleteHandler = (id) => {
    dispatch(deleteUser(id));

    setTimeout(() => {
      window.location.reload();
    }, 1000);
  };

  return (
    <div>
      <div className='flex'>
        <AddModal />
        <AdminSearchBar />
      </div>
      <Table dataSource={users} columns={columns} />;
    </div>
  );
};

export default QuanLyNguoiDung;
