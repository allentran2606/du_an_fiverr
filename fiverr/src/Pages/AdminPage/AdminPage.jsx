import React from "react";
import { Tabs } from "antd";
import QuanLyNguoiDung from "./QuanLyNguoiDung/QuanLyNguoiDung";
import QuanLyCongViec from "./QuanLyCongViec/QuanLyCongViec";
import QuanLyLoaiCongViec from "./QuanLyLoaiCongViec/QuanLyLoaiCongViec";
import QuanLyDichVu from "./QuanLyDichVu/QuanLyDichVu";

const AdminPage = () => {
  const items = [
    {
      key: "1",
      label: "Quan Ly Nguoi Dung",
      children: <QuanLyNguoiDung />,
    },
    { key: "2", label: "Quan Ly Cong Viec", children: <QuanLyCongViec /> },
    {
      key: "3",
      label: "Quan Ly Loai Cong viec",
      children: <QuanLyLoaiCongViec />,
    },
    { key: "4", label: "Quan Ly Dich Vu", children: <QuanLyDichVu /> },
  ];

  return (
    <div className='container'>
      <Tabs tabPosition='left' items={items} />
    </div>
  );
};

export default AdminPage;
