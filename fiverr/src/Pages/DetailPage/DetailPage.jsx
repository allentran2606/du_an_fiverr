import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import DetailGallery from "../../Components/DetailGallery/DetailGallery";
import DetailPlans from "../../Components/DetailPlans/DetailPlans";
import DetailHeader from "../../Components/DetailHeader/DetailHeader";
import {
  getBinhLuanTheoCongViec,
  getCongViecChiTiet,
} from "../../redux-toolkit/thunk/congViecThunk";
import DetailDescription from "../../Components/DetailDescription/DetailDescription";
import DetailFaq from "../../Components/DetailFaq/DetailFaq";
import DetailReviews from "../../Components/DetailReviews/DetailReviews";
import DetailComments from "../../Components/DetailComments/DetailComments";

const DetailPage = () => {
  const dispatch = useDispatch();

  const id = 1;

  useEffect(() => {
    dispatch(getCongViecChiTiet(id));
    dispatch(getBinhLuanTheoCongViec(id));
  }, [dispatch]);

  return (
    <div className="mt-20 container mx-auto">
      <DetailHeader />
      <div className="xl:flex xl:justify-between lg:justify-between md:justify-between p-5">
        <DetailGallery />
        <DetailPlans />
      </div>
      <DetailDescription />
      <DetailFaq />
      <DetailReviews />
      <DetailComments />
    </div>
  );
};

export default DetailPage;
