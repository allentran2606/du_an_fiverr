import * as yup from "yup";

export const registerSchema = yup.object().shape({
  // id: yup.number().required("You must enter your id"),
  name: yup.string().required("You must enter your name"),
  email: yup
    .string()
    .email("Please enter a valid email")
    .required("You must enter an email"),
  password: yup.string().required("You must enter a password"),
  phone: yup.string().required("You must enter your phone number"),
  birthday: yup.date().required("You must enter your birthday"),
  gender: yup.boolean().required("You must choose your gender"),
  // role: yup.string().required("You must enter your role"),
  // skill: yup.string().required("You must enter skills"),
  // certificate: yup.string().required("You must enter your certification"),
});
