import React from "react";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { message } from "antd";
import { boolean } from "yup";
import { registerSchema } from "./schemas/register.js";
import * as Yup from "yup";
import { postDangKy } from "../../redux-toolkit/thunk/userThunk";
import { userService } from "../../services/userService.jsx";

export default function RegisterPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onSubmit = (values, actions) => {
    console.log(values);
    dispatch(postDangKy(values));
    message.success("Sign up success!");
  };

  const { values, errors, touched, handleSubmit, handleBlur, handleChange } =
    useFormik({
      initialValues: {
        id: 0,
        name: "",
        email: "",
        password: "",
        phone: "",
        birthday: "",
        gender: boolean,
        role: "USER",
        // skill: "",
        // certificate: "",
      },
      validationSchema: registerSchema,
      onSubmit,
    });

  return (
    <div className="flex justify-center min-h-screen text-left">
      {/* Image */}
      <div
        className="hidden bg-cover lg:block lg:w-2/5"
        style={{
          backgroundImage:
            "url(https://images.unsplash.com/photo-1494621930069-4fd4b2e24a11?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=715&q=80)",
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
      ></div>
      {/* Form */}
      <div className="flex items-center w-full max-w-3xl mx-auto p-8 lg:px-12 lg:w-3/5">
        <div className="w-full">
          <h1 className="text-3xl font-bold">Let's sign up!</h1>
          <p className="mt-4">
            Let’s get you all set up so you can verify your personal account and
            begin setting up your profile.
          </p>
          <form className="bg-white text-left" onSubmit={handleSubmit}>
            <div className="grid grid-cols-1 gap-6 mt-8 md:grid-cols-2">
              <div>
                <label className="block mb-2 text-sm font-bold">Name</label>
                <input
                  type="text"
                  name="name"
                  id="name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.name}
                  placeholder="John Cena"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">
                  Phone number
                </label>
                <input
                  type="text"
                  name="phone"
                  id="phone"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phone}
                  placeholder="XXXX-XXX-XXX"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Password</label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  placeholder="Enter your password"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Email</label>
                <input
                  type="email"
                  name="email"
                  id="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  placeholder="siuuuuu@gmail.com"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div>
              {/* <div>
                <label className='block mb-2 text-sm font-bold'>
                  Confirm password
                </label>
                <input
                  type='password'
                  placeholder='Enter your password'
                  className='block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md'
                />
              </div> */}
              <div>
                <label className="block mb-2 text-sm font-bold">Birthday</label>
                <input
                  type="date"
                  name="birthday"
                  id="birthday"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.birthday}
                  placeholder="John"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Gender</label>
                <div className="flex items-center">
                  <div className="flex items-center mr-6">
                    <input
                      type="checkbox"
                      name="gender"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.gender}
                      placeholder="John"
                      className="block w-full mr-1 bg-white border border-gray-400 rounded-md"
                    />
                    <span>Male</span>
                  </div>

                  <div className="flex items-center">
                    <input
                      type="checkbox"
                      name="gender"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.gender}
                      placeholder="John"
                      className="block w-full mr-1 bg-white border border-gray-400 rounded-md"
                    />
                    <span>Female</span>
                  </div>
                </div>
              </div>
              {/* <div>
                <label className='block mb-2 text-sm font-bold'>Role</label>
                <input
                  type="text"
                  name="role"
                  id="role"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.role}
                  placeholder="Seller"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Skill</label>
                <textarea
                  type="text"
                  name="skill"
                  id="skill"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.skill}
                  placeholder="Leading, Communication"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div>
              <div>
                <label className='block mb-2 text-sm font-bold'>
                  Certification
                </label>
                <textarea
                  type="text"
                  name="certificate"
                  id="certificate"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.certificate}
                  placeholder="MBA"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div> */}
            </div>
            <button
              className="flex items-center justify-between w-full px-6 py-3 text-sm tracking-wide text-white capitalize transition-colors duration-300 transform bg-green-400 rounded-md hover:bg-green-600 mt-6"
              type="submit"
            >
              Sign Up
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
