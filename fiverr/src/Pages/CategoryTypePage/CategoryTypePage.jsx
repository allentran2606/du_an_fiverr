import React from "react";
import CategoryMenu from "../../Components/CategoryMenu/CategoryMenu";
import HeaderAlt from "../../Components/Header/HeaderAlt";
import JobList from "../../Components/JobList/JobList";
import SearchFilter from "../../Components/SearchFilter/SearchFilter";
import Footer from "../../Components/Footer/Footer";

export default function CategoryTypePage() {
  return (
    <div>
      <HeaderAlt />
      <CategoryMenu />
      <SearchFilter />
      <JobList />
      <Footer />
    </div>
  );
}
