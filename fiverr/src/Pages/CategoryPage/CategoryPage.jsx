import React from "react";
import { useParams } from "react-router-dom";
import HeaderAlt from "../../Components/Header/HeaderAlt";
import CategoryMenu from "../../Components/CategoryMenu/CategoryMenu";
import CategoryBanner from "../../Components/CategoryBanner/CategoryBanner";
import CategoryCarousel from "../../Components/CategoryCarousel/CategoryCarousel";
import Explore from "../../Components/Explore/Explore";
import SubFooter from "../../Components/SubFooter/SubFooter";
import Footer from "../../Components/Footer/Footer";

export default function CategoryPage() {
  const param = useParams();
  return (
    <div>
      <HeaderAlt />
      <CategoryMenu />
      <CategoryBanner />
      <CategoryCarousel />
      <Explore param={param} />
      <SubFooter />
      <Footer />
    </div>
  );
}
