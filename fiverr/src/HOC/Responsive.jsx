import { useMediaQuery } from "react-responsive";

export const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 1241 });
  return isDesktop ? children : null;
};

export const Laptop = ({ children }) => {
  const isLaptop = useMediaQuery({ minWidth: 1061, maxWidth: 1240 });
  return isLaptop ? children : null;
};

export const Tablet = ({ children }) => {
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 1060 });
  return isTablet ? children : null;
};

export const Mobile = ({ children }) => {
  const isMobile = useMediaQuery({ minWidth: 640, maxWidth: 767 });
  return isMobile ? children : null;
};
